const ethers = require('ethers');
const dotenv = require("dotenv")
const pancakeSwapRouter = require("./ABI/pancakeswap_router.json")
dotenv.config()

const mnemonic = process.env.MY_MNEMONIC
const provider = new ethers.providers.JsonRpcProvider("https://bsc-dataseed.binance.org/")
const wallet = new ethers.Wallet(mnemonic);
const account = wallet.connect(provider);

const router = new ethers.Contract(
    process.env.PANCAKESWAP_ROUTER,
    pancakeSwapRouter.ABI,
    account
);

const data = {

    AMOUNT_OF_BNB: 0.012, // how much you want to buy in BNB

    Slippage: 25, //in Percentage

    gasPrice: ethers.utils.parseUnits("5", 'gwei'), //in gwei

    gasLimit: 450000, //at least 21000

}

const buyAction = async () => {

    try {
        let amountOutMin = 0;

        const amountIn = ethers.utils.parseUnits(`${data.AMOUNT_OF_BNB}`, 'ether');

        const amounts = await router.getAmountsOut(amountIn, [process.env.WBNB, process.env.BUY_TOKEN_ADDRESS]);

        amountOutMin = amounts[1].sub(amounts[1].div(`${data.Slippage}`))

        const tx = await router.swapETHForExactTokens( //uncomment here if you want to buy token
            amountOutMin,
            [process.env.WBNB, process.env.BUY_TOKEN_ADDRESS],
            process.env.RECIPIENT_ADDRESS,
            Date.now() + 1000 * 60 * 5, //5 minutes
            {
                'gasLimit': data.gasLimit,
                'gasPrice': data.gasPrice,
                'nonce': null, //set you want buy at where position in blocks
                'value': amountIn
            });
        
        const receipt = await tx.wait();
        console.log(`Transaction receipt : https://www.bscscan.com/tx/${receipt.logs[1].transactionHash}`);

    } catch (error) {
        console.log("Transaction Error: " + error);
    }
}
try {

    buyAction()
    
} catch (error) {
    console.log(error)
}
