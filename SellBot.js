const ethers = require('ethers');
const dotenv = require("dotenv")
const ercABI = require("./ABI/ERC20.json")
const pancakeSwapRouter = require("./ABI/pancakeswap_router.json");
dotenv.config()

const mnemonic = process.env.MY_MNEMONIC
const provider = new ethers.providers.JsonRpcProvider("https://bsc-dataseed.binance.org/")
const wallet = new ethers.Wallet(mnemonic);
const account = wallet.connect(provider);


const router = new ethers.Contract(
    process.env.PANCAKESWAP_ROUTER,
    pancakeSwapRouter.ABI,
    account
);

const data = {

    AMOUNT_OF_TOKEN: 0.001, 

    Slippage: 12, //in Percentage

    gasPrice: ethers.utils.parseUnits("5", 'gwei'), //in gwei

    gasLimit: 450000, //at least 21000

}


const erc = new ethers.Contract(
    process.env.BUY_TOKEN_ADDRESS,
    ercABI.ABI,
    account
);

async function getBalance() {
  const result = await erc.balanceOf(process.env.RECIPIENT_ADDRESS) // 29803630997051883414242659
  //const format = ethers.utils.fromWei(result); // 29803630.997051883414242659
  console.log(ethers.utils.formatUnits(result, 18))
  //console.log(JSON.stringify(result));
}
//getBalance()
const sellAction = async () => {

    try {
        let amountOutMin = 0;
        //We buy x amount of the new token for our bnb
        const amountIn = ethers.utils.parseUnits(`${data.AMOUNT_OF_TOKEN}`, 'ether');
        const amounts = await router.getAmountsOut(amountIn, [process.env.BUY_TOKEN_ADDRESS,process.env.WBNB ]);
        
        //Our execution price will be a bit different, we need some flexibility
        amountOutMin = amounts[1].sub(amounts[1].div(`${data.Slippage}`))
        //console.log("+++"+typeof(JSON.stringify(amountOutMin))+" "+amountOutMin)
        // const tx = await router.swapExactTokensForTokensSupportingFeeOnTransferTokens( //uncomment this if you want to buy deflationary token
        const tx = await router.swapExactTokensForETH( //uncomment here if you want to buy token
            amountIn,
            amountOutMin,
            [process.env.BUY_TOKEN_ADDRESS,process.env.WBNB],
            process.env.RECIPIENT_ADDRESS,
            Date.now() + 1000 * 60 * 25, //5 minutes
            {
                "value":amountIn,
                "gasLimit": data.gasLimit,
                'gasPrice': data.gasPrice,
            });
        
        const receipt = await tx.wait();
        console.log(`Transaction receipt : https://www.bscscan.com/tx/${receipt.logs[1].transactionHash}`);

    } catch (error) {
        console.log("Transaction Error: " + error);
        //const s=JSON.stringify(error)
    }
}

try {
    sellAction()
} catch (error) {
    console.log("sss"+error)
}